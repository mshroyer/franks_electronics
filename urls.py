from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'franks_electronics.views.home', name='home'),
    # url(r'^franks_electronics/', include('franks_electronics.foo.urls')),
    url(r'^$', 'tickets.views.index'),
    url(r'^tickets/search/', 'tickets.views.search'),
    url(r'^tickets/number/(?P<number>\d+)/$', 'tickets.views.number'),
    url(r'^tickets/voice/$', 'tickets.views.voice'),
    url(r'^tickets/id/(?P<ticket_id>\d+)/$', 'tickets.views.detail'),

    url(r'^services/$', 'django.views.generic.simple.direct_to_template', {'template': 'tickets/services.html'}),
    url(r'^about/$', 'django.views.generic.simple.direct_to_template', {'template': 'tickets/about.html'}),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
