// Email address de-obfuscator
function unobfuscateAt(id, obfAddr, proto) {
    var ele = document.getElementById(id);

    var addrAry = new Array(obfAddr.length);
    for ( var i = 0; i < obfAddr.length; i++ )
        addrAry[i] = obfAddr.charAt(i);

    for ( var i = 0; i < addrAry.length - 1; i += 2 ) {
        temp = addrAry[i];
        addrAry[i] = addrAry[i+1];
        addrAry[i+1] = temp;
    }
    addr = addrAry.join('');

    ele.innerHTML = '<a href="' + proto + ':' + addr + '">' + addr + '</a>';
}

function unobfuscateEmailAt(id, obfAddr) {
    unobfuscateAt(id, obfAddr, 'mailto');
}

function unobfuscateJabberAt(id, obfAddr) {
    unobfuscateAt(id, obfAddr, 'xmpp');
}
