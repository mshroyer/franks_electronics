from django.db import models
from django.db.models.signals import post_save
from decimal import Decimal

class CurrencyField(models.DecimalField):
    __metaclass__ = models.SubfieldBase

    def to_python(self, value):
        try:
            return super(CurrencyField, self).to_python(value).quantize(Decimal("0.01"))
        except AttributeError:
            return None

STATUS_DETAIL = (
    ('C', 'Created', 'The ticket has been created but has not been entered into the estimate queue.'),
    ('I', 'In estimate queue', 'The item has been signed in and is currently in queue for the issue to be diagnosed and an estimate to be created.'),
    ('E', 'On bench for estimate', 'The item is on the bench and the issue is being diagnosed. Repair parts are being researched and an estimate is being created.'),
    ('T', 'To be called', 'The estimate of the repair has been completed and a phone call has been scheduled. You should recieve a phone call today.'),
    ('W', 'Waiting for estimate approval', 'The estimate of the repair has been completed and the customer has been called. We are currently awaiting for an approval for repair.'),
    ('Q', 'In repair queue', 'The estimate has been approved by the customer and parts are on order. The item is in queue to be repaired when parts arrive.'),
    ('R', 'On bench for repair', 'The item is on the bench and the issue is being repaired.'),
    ('B', 'Burn in', 'The item has been repaired and is going through rigorous testing.'),
    ('U', 'To be picked up', 'The item is ready to be picked up.'),
    ('L', 'Closed', 'The item has been picked up and the repair ticket has been closed.'),
)

STATUS_CHOICES = tuple(x[0:2] for x in STATUS_DETAIL)

class Ticket(models.Model):
    timestamp = models.DateTimeField(auto_now=True, auto_now_add=True)
    number = models.IntegerField()
    customer = models.CharField(max_length=128, blank=True)
    make = models.CharField(max_length=255, blank=True, verbose_name="Item make")
    model = models.CharField(max_length=255, blank=True, verbose_name="Item model")
    serial_number = models.CharField(max_length=255, blank=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default='S')
    notes = models.TextField(blank=True)
    private_notes = models.TextField(blank=True)
    #estimate = models.DecimalField(null=True, decimal_places=2, max_digits=16)
    estimate = CurrencyField(blank=True, null=True, decimal_places=2, max_digits=16)
    estimate_date = models.DateField(blank=True, null=True)
    approved = models.BooleanField(verbose_name="Estimate approved")
    approval_date = models.DateField(blank=True, null=True)
    completion_date = models.DateField(blank=True, null=True)

    @property
    def status_detail(self):
        for detail in STATUS_DETAIL:
            if detail[0] == self.status:
                return detail[2]
        return ''

    @property
    def estimate_display(self):
        result = self.estimate
        return "$%s" % result if result else ''

    @property
    def estimate_date_display(self):
        result = self.estimate_date
        return result if result else ''

    @property
    def approved_display(self):
        return 'Yes' if self.approved else ''

    @property
    def approval_date_display(self):
        result = self.approval_date
        return result if result else ''

    @property
    def completion_date_display(self):
        result = self.completion_date
        return result if result else ''

    def latest_status_log(self):
        try:
            return StatusLog.objects.filter(ticket=self).latest()
        except StatusLog.DoesNotExist:
            return None

    def log_status(self):
        lastlog = self.latest_status_log()
        if lastlog is None or lastlog.status != self.status:
            newlog = StatusLog(ticket=self)
            newlog.status = self.status
            newlog.save()

    def get_absolute_url(self):
        return "/tickets/id/%i/" % self.id

    def save(self, *args, **kwargs):
        super(Ticket, self).save(*args, **kwargs)
        self.log_status()

    def __unicode__(self):
        return "Ticket #%d" % self.number
        
    class Meta:
        ordering = ['-timestamp']
        verbose_name = "service ticket"
        verbose_name_plural = "service tickets"

class ContactLog(models.Model):
    ticket = models.ForeignKey(Ticket)
    timestamp = models.DateTimeField()
    status = models.TextField()

    def __unicode__(self):
        return self.status

    class Meta:
        ordering = ['ticket', '-timestamp']
        verbose_name = "customer contact history"
        verbose_name_plural = "customer contact history"
        get_latest_by = 'timestamp'

class StatusLog(models.Model):
    ticket = models.ForeignKey(Ticket)
    timestamp = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default='S')

    def __unicode__(self):
        return "Ticket #%d status log at %s" % (self.ticket.number, str(self.timestamp))

    class Meta:
        ordering = ['ticket', '-timestamp']
        verbose_name = "ticket status history"
        verbose_name_plural = "ticket status history"
        get_latest_by = 'timestamp'
