from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response, redirect, get_object_or_404
from tickets.models import Ticket
from django import forms
from django.utils.translation import ugettext as _
from django.conf import settings
from django.template import RequestContext

class SearchForm(forms.Form):
    number = forms.IntegerField(min_value=1)

    def clean_number(self):
        n = self.cleaned_data['number']
        try:
            self.cleaned_data['ticket'] = Ticket.objects.get(number=n)
        except Ticket.DoesNotExist:
            raise forms.ValidationError("Ticket #%d could not be found." % n)

def index(request):
    return render_to_response('tickets/index.html', {'form': SearchForm()}, context_instance=RequestContext(request))

def search(request):
    if 'number' in request.GET:
        form = SearchForm(request.GET)
        if form.is_valid():
            return redirect(form.cleaned_data['ticket'])
        else:
            return render_to_response('tickets/search.html', {'form': form}, context_instance=RequestContext(request))
    else:
        return render_to_response('tickets/search.html', {'form': SearchForm()}, context_instance=RequestContext(request))

def number(request, number):
    ticket = get_object_or_404(Ticket, number=number)
    return redirect(ticket)

def voice(request):
    try:
        number = request.GET.get('number', None)
        if number is not None:
            ticket = Ticket.objects.get(number=number)
            response_text = ticket.status_detail
        else:
            response_text = "No ticket number specified"
    except Ticket.DoesNotExist:
        response_text = "Invalid ticket number"
    return HttpResponse(response_text)

def detail(request, ticket_id):
    ticket = get_object_or_404(Ticket, id=ticket_id)
    return render_to_response('tickets/ticket.html', {
        'ticket': ticket,
        'show_history': settings.SHOW_HISTORY,
    }, context_instance=RequestContext(request))
