from tickets.models import Ticket, StatusLog, ContactLog
from django.contrib import admin
from django.utils.text import capfirst, get_text_list
from django.utils.translation import ugettext as _
from django.utils.encoding import force_unicode

class ContactLogInline(admin.TabularInline):
    model = ContactLog
    extra = 1
    fields = ('timestamp', 'status')

class StatusLogInline(admin.TabularInline):
    model = StatusLog
    extra = 0
    fields = ('timestamp', 'status')
    readonly_fields = fields

class TicketAdmin(admin.ModelAdmin):
    inlines = [ContactLogInline, StatusLogInline]
    list_display = ('number', 'customer', 'status', 'timestamp')
    list_filter = ('timestamp', 'status')
    search_fields = ('number', 'customer', 'notes')

    def construct_change_message(self, request, form, formsets):
        """
        Construct a change message from a changed object, logging what we
        changed the given values too.
        """
        change_message = []
        if form.changed_data:
            changed_values = ['%s to %s' % (key, form[key].value()) for key in form.changed_data]
            change_message.append(_('Changed %s.') % get_text_list(changed_values, _('and')))

        if formsets:
            for formset in formsets:
                for added_object in formset.new_objects:
                    change_message.append(_('Added %(name)s "%(object)s".')
                                          % {'name': force_unicode(added_object._meta.verbose_name),
                                             'object': force_unicode(added_object)})
                for changed_object, changed_fields in formset.changed_objects:
                    change_message.append(_('Changed %(list)s for %(name)s "%(object)s".')
                                          % {'list': get_text_list(changed_fields, _('and')),
                                             'name': force_unicode(changed_object._meta.verbose_name),
                                             'object': force_unicode(changed_object)})
                for deleted_object in formset.deleted_objects:
                    change_message.append(_('Deleted %(name)s "%(object)s".')
                                          % {'name': force_unicode(deleted_object._meta.verbose_name),
                                             'object': force_unicode(deleted_object)})
        change_message = ' '.join(change_message)
        return change_message or _('No fields changed.')
        
    class Media:
        css = {
            "all": ("css/admin_ticket.css",)
        }

admin.site.register(Ticket, TicketAdmin)
