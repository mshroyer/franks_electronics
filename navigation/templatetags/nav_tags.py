from django import template

register = template.Library()

@register.simple_tag
def nav_list(request):

    def nav_item(name, url, active_test):
        item_html = '<li><a href="%s" class="' % url
        if active_test(request.path):
            item_html += 'current'
        item_html += '">%s</a></li>' % name
        return item_html
        
    html  = nav_item("Home", "/", lambda p: p == "/")
    html += nav_item("Services", "/services/", lambda p: p == "/services/")
    html += nav_item("Repair Status", "/tickets/search/", lambda p: p.startswith("/tickets/"))
    html += nav_item("About", "/about", lambda p: p == "/about/")
    
    return html
